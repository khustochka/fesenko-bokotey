$(function() {
		$( ".sortable" ).sortable({
            connectWith: ".sortable",
            tolerance: "pointer",
			placeholder: "ui-state-highlight",
            forcePlaceholderSize: true,
            update: function() { saveOrder($( this )); }
		});
		$( "#sortable" ).disableSelection();
});

function saveOrder(el) {
    result = {data: el.sortable("toArray"), parent: el.attr('id')};
    $.ajax({
          url: window.location.pathname,
          type: "POST",
          data: JSON.stringify(result),
          processData: false,
          contentType: "application/json; charset=utf-8"
    })
}