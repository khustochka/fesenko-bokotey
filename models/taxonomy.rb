require 'bundler'
Bundler.require

MongoMapper.database = 'fesenko'

class Family
  include MongoMapper::Document

  key :order, String
  key :name, String
  key :index_num, Integer

  many :species
end

class Species
  include MongoMapper::EmbeddedDocument

  key :data, Hash
  key :index_num, Integer

  belongs_to :family
end
