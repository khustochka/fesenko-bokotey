require 'sinatra'
require 'haml'
require './models/taxonomy'

get '/' do
  haml :panel
end

get '/families' do
  haml :families
end

get '/species' do
  haml :species
end

post '/families' do
  families = JSON.parse(request.body.read)['data']
  families.each_with_index do |name, i|
    fam = Family.first(:name => name, :index_num.ne => (i + 1))
    fam.set(:index_num => i + 1) unless fam.nil?
  end
end

post '/species' do
  data = JSON.parse(request.body.read)
  family = Family.first(:name => data['parent'])
  species = data['data']
  fam_sp = family.species
  new_species = species.map do |id|
    fam_sp.detect { |s| s.id == BSON::ObjectId(id) }
  end
  family.species = new_species
  family.save!
end
